* [Install Docker on Linux](https://docs.docker.com/engine/install/ubuntu/#installation-methods) : *Recommended* Option worked for me.
* [Run docker in rootless mode on Linux](https://docs.docker.com/engine/security/rootless/) : This is default on macOS

Documentation is quite detailed and clear. 💯

## (mini) Cheat sheet

* List dockers container, images via `docker ps -a`. This lists all the containers even if they are stopped.
* `docker ps` shows only the running containers
* Remove images (To clean up disk space) using `docker rm -f <name>` (Name is the last column in `docker ps` output)
* `docker <system/image/container/network> prune` : Removed unused objects.
  * Use `system` option to "Remove all unused containers, networks, images (both dangling and unreferenced), and optionally, volumes."

Some specific commands in [[Postgres]]