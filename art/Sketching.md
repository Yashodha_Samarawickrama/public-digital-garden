# Resources

* [Beginner's Guide to Sketchbook Pro](https://design.tutsplus.com/tutorials/the-beginners-guide-to-sketchbook-pro--cms-30592)
* [Deviant Muro](https://www.deviantart.com/muro/)
* [Ctrl+paint](https://www.ctrlpaint.com/getting-started) : Free courses
