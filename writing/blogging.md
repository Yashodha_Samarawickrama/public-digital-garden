## Micro Blog, Notes, Webmentions etc.

* [Good write up - 3 part series](https://keithjgrant.com/posts/2019/01/low-friction-workflow-for-notes/)
* [Link to Shindig theme used by Keith Grant ☝️](https://github.com/keithjgrant/keithjgrant.com/tree/master/themes/shindig)

## Learning Resources

* [Hashnode bootcamp III recordings](https://townhall.hashnode.com/hashnode-bootcamp-iii-summary-session-recordings-tasks-and-certificates)
* [Workflow from Voice typing to publishing](https://bridgetwillard.com/correctly-format-blog-post/)
  * I get most of the features of the Hemingway App (mentioned in the article) from emacs like: 
    * Fluff word detection
    * Passive voice
    * Word count
    * Autocompletion for prose, not sure if Hemingway app has that.
    * Spellcheck
    * Grade Level score (via `M-x writegood-grade-level`)

## Blog Header Image Generator

* [Slickr](https://slickr.vercel.app/app)

## Using unsplash images into the blog

I don't want to download-and-reduce-size of the photo on my machine, just to upload it to the blog.
It is better that unsplash API take care of it.

* Copy the last part of the URL for the photo you like i.e. part after `https://unsplash.com/photos/<this-is-what-we-want>`
* Use [source api](https://source.unsplash.com/) to get the "direct" URL of the photo.  Append the "last part" followed by the dimensions (like `widthXheight`) to create the URL
* Now use this URL.

## Using random images

* Finding appropriate image can sometime take longer than writing the blog post itself.
* Enter [Lorem Ipsum for Photos](https://picsum.photos/) - Seems feature complete, like define size in URL itself, select specific image etc.

## `mkdocs` site on netlify

I use `pipenv` for my normal workflow, but it may not work on Netlify.

Following worked for me (based on their documentation)

* Create `requirements.txt`. I added same contents as my `Pipfile`
* Created `runtime.txt` - added `3.6` to it. (Default is `2.7`)
* Created a separate branch `netlify` (I didn't want Netlify specific files in
   master)
* Rest is easy. Select git provider and branch name. The *wizard* recognized that this was `mkdocs` site, and automatically filled in `build command` and `publish` directory. Awesome !!


### Custom domain on gitlab pages

(Oct 11, 2018)

[This document](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html) is really detailed.  I had some trouble getting the verification, but it may have been DNS propagation issue. (*Also, I think gitlab is generally slow*)
