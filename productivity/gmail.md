# Keyboard shortcuts ⌨️

First *Turn on* Keyboard shortcuts from `Settings`

Some common ones :

Action| Shortcut
-------|------
Delete | #
Select a conversation | x
Mark as read | Shift+i

[Reference](https://support.google.com/mail/answer/6594?co=GENIE.Platform%3DDesktop&hl=en#zippy=%2Cactions)
