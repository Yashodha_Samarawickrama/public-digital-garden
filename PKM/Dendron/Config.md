## Getting multi-vault setup to work with selective publishing

As of this writing multi-vault and selective publishing (probably) do not work
together.

I was able to get it to work (sorta hack)
in `dendron.yml` I have: 

```yml
    duplicateNoteBehavior:
      action: "useVault"
      payload: [public, private]
```

My private vault, which I did not want published essentially has `daily.journal....` files. Besides the default `root.md` and `dendron.md` and `dendron.welcome.md`

`dendron*` have `nav_exclude` set to true, so I think that is automatically taken care of.
I might delete these two files from private vault anyway.

During site generation,  due to `writeStubs: true` - `daily.md` got created.

What I did was added `published: false` to it's front matter, so entire daily Hierarchy is now "hidden" from publishing. 🎉

## Dendron for Blogging

[See this documentation](https://dendron.so/notes/6d21fd60-e564-4a55-b4c0-d2586293b908.html)

## Same note under multiple hierarchies

```yaml
title: "Hierarchy Fist Note Taking"
hierarchy:
 - note.methodes
 - pkm.methodes
 ```
