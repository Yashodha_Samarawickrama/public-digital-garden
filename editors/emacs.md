## Generic Emacs

Following should work for any "configuration" of emacs, although I have only
tried these with doom.

### Emacs in a terminal

Lately, I've started using emacs in a terminal using `emacs -nw`
But I came across a problem that `M-x` key won't work. i.e. emacs never receives this keystroke.

Turns out this has to do with how the terminal handles keystrokes.
~~But at least I found a workaround. If I type <kbd>Esc</kbd> followed by `x` then
Emacs receives `M-x`.~~ FWIW, it worked only once. 🤷‍️

Ref: [This SO question](https://stackoverflow.com/q/38553213) and 
[last bullet point in this explanation](https://emacs.stackexchange.com/a/13957)

### Find path to a binary

In terminal, one uses `which <binaryfile>` to find the path of the binary (in
case shell picks wrong path, or if binary is not found)

The emacs equivalent is `M-; (executable-find "binary-file-name")`

I've found this helpful when troubleshooting.

*Thanks to doom-emacs discord channel**

*Apparently, `M-! which <binaryfile>` also works, it essentially calls `which`
command*

### Emacs Everywhere

Do you like emacs so much that you wish you could use it in any edit field on any site on the internet ?
Now you can.

Try [Emacs everywhere](https://github.com/zachcurry/emacs-anywhere)
I tried it with Doom emacs - works well.


### Push branch using `magit`

`M-x magit-push-matching` seems closest to `git push origin HEAD`

There are other `magit-push-..` commands that provide better control over where you want to push.

Read the [Magit Manual](https://magit.vc/manual/magit/Pushing.html) for details.

### Split hunk using magit

When using `git add -p` functionality from the terminal, I use `s` i.e. split
hunk functionality a lot. But `magit` apparently does not have it "out of the
box"

One can achieve the same using emacs' "select lines"

In `magit-status` buffer, navigate to the line/s you want to commit. Then using
<kbd>Ctrl+Shift+2</kbd> i.e. `C-@`, select the line/s. Stage only those lines using <kbd>s</kbd>.

### Rename a file

`M-x rename-file` (Works in Emacs 26+)


## Doom Emacs

### Change Doom logo

1. Copy a logo file to some folder like say `misc` under `~/.doom.d`
2. Add this to your `config.el`. Save and restart via `SPC q r`

```
(setq fancy-splash-image (concat doom-private-dir "misc/logo.png"))
```

(Found this via Doom Emacs Telegram channel)

### Delete branches using `magit`

Doom has different keybindings, hence this section is under Doom emacs

1. `M-x magit-show-refs`
2. Go to the `Branches` section
3. `x` (for `magit-delete-thing`)
4. `RET`
5. `y` to confirm

### LSP mode for elixir

Get `elixir-ls` locally :

```
git clone https://github.com/elixir-lsp/elixir-ls.git
cd elixir-ls
mix deps.get
mix elixir_ls.release
```

This will create a `release/language_server.sh` (and `.bat` for windows) file that
you will need for the Emacs integration, so note down this path (referred to
later as `path-to-elixir-ls/release`).

(Above instructions from [elixir forum](https://elixirforum.com/t/emacs-elixir-setup-configuration-wiki/19196))

In your `init.el` enable `lsp` in `:tools` section.

Then in your `config.el` Add the following two lines: 
```
(add-hook 'elixir-mode-hook #'lsp!)
(add-to-list 'exec-path "path-to-elixir-ls/release")
```

(Above instructions from doom discord channel. Now may be outdated. Depending on when you read this)
