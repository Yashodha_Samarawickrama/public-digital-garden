Helix is modal text editor written in Rust.
It is similar to neo/vi/m except that the editing is inspired by Kakoune editor

To paraphrase the documentation:

> Helix's editing model is strongly inspired from vim and kakoune, 
> and a notable difference from vim (and the most striking similarity to kakoune) 
> is that Helix follows the selection → action model. This means that the whatever
> you are going to act on (a word, a paragraph, a line, etc) is selected first 
> and the action itself (delete, change, yank, etc) comes second.

If you are coming from neo/vi/m, this takes getting used to.
Retraining the muscle memory is not easy.

But main reason I'm trying to use more Helix (This entry is created in helix editor) 
is because the editor "just works", it comes with built-in LSP, built-in themes, and a whole lot more.


* [Migrating from Vim](https://github.com/helix-editor/helix/wiki/Migrating-from-Vim)

### Useful Keyboard shortcuts (for me 😄)

* Windows options are available with `Ctrl-w` 
  * `s` for horizontal bottom split
  * `v` for vertical split
  * `q` for close window
* Global search inside workspace folder : `SPC /`
* Equivalent of `Shift D` in vim :
  * Select till end of the line `t<ret>`
    * `t` means `till`
    * `<ret>` in `Enter` keystroke, which matches newline, so end of the line
  * now `d` for delete. 
* Find and replace (equivalent of `%s/<old>/<new>/g`)
  * Select entire file with `%`
  * `s` to search the string
  * `c` to replace it with new string
* Navigating the source code
  * `gd` : Go to Definition
  * `gp` : Go to Previous buffer (This is equivalent of `Ctrl-O` to go back up the stack)

### How to create a new file from within Helix ?

>:open (or just :o) <new file path starting from active directory>

This gives you auto-completion while writing the path.

Refer to this original thread on [reddit](https://www.reddit.com/r/HelixEditor/comments/107mpx8/comment/j3ne90h/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button)
