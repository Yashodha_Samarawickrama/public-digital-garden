---
title: "About"
created: 2021-07-31
modified: 2023-06-18
---

This is my ~~third~~ fourth attempt at sharing my _notes-in-public_

The first one was [dev notes](https://devnotes.desipenguin.com/). This was retired in favour of current [Tiddlywiki](https://wiki.desipenguin.com/)

While Tiddlywiki (or TW) is awesome, it still uses somewhat non-standard text format. TW is a _CMS-in-a-file_
Adding a _tiddler_ as they call new page may not be impossible using standard editor, it is _damn_ difficult. (I'm sure there is an Emacs plugin somewhere to edit TW 😜)

This site itself moved from [Dendron](https://wiki.dendron.so/) to [Fork of Obsidian-zola called cheap-publish](https://github.com/orditeck/cheap-publish) - so that is considered fourth attempt.
The content remained the same, I just needed to remove Dendron specific front matter, and rename the files to hierarchical folder structure.

---

Following is from the landing page of my dev-notes. Since most of it still applies, I decided to share it here as-is.

> I used to keep my "notes" in a bunch of `org` files.
> (and before that - in `md` and `rst` files - couldn't decide between the format. Finally, when I switched to Emacs, `org` files won.)
>
> From time to time, I considered sharing those openly, but they were mostly "notes", very unstructured. But more importantly, I thought others may not be able to make sense, or may not find it useful.
>
> Then I came across [this](https://devnotes.johan-martin.com) via Google search, no less.
>
> And I realized, why should I worry whether it is useful for anyone.

The irony of moving to `org` from `md` ☝️ only to come back to Markdown (here) is not lost on me.

While both `org` and `md` are essentially text formats, `org` is more at home (mostly) with emacs. Sure there are some other editors that support `org` format, but it is not as widely supported as `md`

I know that Markdown in itself is not a standard, most of what I use here should be lowest common denominator, and should be supported everywhere.

## My other blogs

- [Micro blog](https://microblog.desipenguin.com/) - Most active these days
- [Learn. Share. Improve.](https://learnings.desipenguin.com/)
- [Desi penguin's blog](https://mandarvaze.github.io/)
