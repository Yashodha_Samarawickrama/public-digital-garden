## Windows-Ubuntu interop
* Access path from inside wsl in Windows as `\\wsl.localhost\Ubuntu-22.04\home\username\path`
* Copy/Paste does not work out of the box
  * Enable it by opening the properties panel. You can access it by right-clicking the window pane header
    * Then select the “**Use Ctrl+Shift+C/V as Copy / Paste**” option

## Setting up helix Editor
* Need to add `true-color = true` **manually** in config.toml
  * Else `toykostorm-night` theme was not supported
* Whitespace characters did not work
  * Need nerd font installed in Windows (Not inside Ubuntu in WSL)
  * Then from Properties menu, select the correct Font
    * Tried various fonts like SourceCodePro, Iosevka Curly Slab, FiraCode Nerd
    * Finally [Cascadia Code](https://github.com/microsoft/cascadia-code/releases/tag/v2111.01) worked

## Setting up `rust`
* In order for `cargo install` to work, need to have compiler and linker.
  * Fix this via `sudo apt install build-essential`
* `zellij` needed to be installed as `cargo install --locked zellij`
* To open rust documentation from inside wsl, need to set the following env. variable
	* `export BROWSER="/mnt/c/Program Files/Mozilla Firefox/firefox.exe"`


[[Helix]]
[[rust]]
