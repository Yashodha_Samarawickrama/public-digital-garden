## How to add Marathi Phonetic Keyboard to Windows ?
* First add Marathi language via settings as Preferred Language
  * Make sure it is last in the list
* It will install the default keyboard which is INSCRIPT (Difficult to use, unless you know what you are doing)
* Click on `Marathi` under `Preferred Langauges` section, and Click on `Option`
* From `Add a keyboard` section, add `Marathi Phonetic`, and remove INSCRIPT
* Now switch languages via language bar option in task bar
* Refer to this [documentation](https://support.microsoft.com/en-us/windows/how-to-set-up-and-use-indic-phonetic-keyboards-7c4d2e8a-abf2-f200-9866-1a4cead7b127)

## Open emoji Keyboard

`Windows + .` will open emoji keyboard.

## Power Toys

These Power Toys should be shipped by default, rather than having to install
as separate app.

Some cool ones:

- Text Extractor (Not tried yet) : As name suggested, select an image with
text, and this tool with OCR it and return plain text.
- Past as plain text : I used to install Raycast on macOS just for this functionality alone 😊
- Mouse finder: Equivalent functionality is built into macOS
