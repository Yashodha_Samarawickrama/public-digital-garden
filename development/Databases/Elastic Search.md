* Paginate Search results
  * ES returns 10 results by default
  * Use `size` to increase (or decrease) that.
  * Use `from` for getting next pages
  * [Reference](https://www.elastic.co/guide/en/elasticsearch/reference/current/paginate-search-results.html)

* Find all indices: `GET /_cat/indices`
