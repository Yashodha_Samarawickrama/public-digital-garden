## Browse from the Terminal

### w3m

* `brew install w3m` (or apt-get or pacman or appropriate command for your OS/distribution)
* `w3m <URL>`
* Use arrows to traverse the page.
* Press <kbd>Enter</kbd> on a text or edit field. You'll see a prompt to type in your text.
* Press <kbd>Enter</kbd> to submit. (Duh!)
* Use <kbd>Shift+b</kbd> to go back. (*Shift+Down Arrow also worked*)

### Search from terminal

Install [`ddgr`](https://github.com/jarun/ddgr) or [`googler`](https://github.com/jarun/googler) then

`BROWSER=w3m ddgr query`

This is helpful in reducing the distraction. 
To be honest, search results view is not the best viewing experience. So one is not tempted to hang-around. Just find what you were looking for and get out 😆

### tmux

* [Chris Toomey's tmux course on Upcase](https://thoughtbot.com/upcase/tmux)
* [Nord Theme for tmux](https://github.com/arcticicestudio/nord-tmux)
* [Awesome tmux](https://github.com/rothgar/awesome-tmux)
* [Oh My tmux](https://github.com/gpakosz/.tmux)
* (My) Cheat sheet
  * Set `prefix` to <kbd>Ctrl+S</kbd>
  * Scroll thru the buffer using <kbd>Ctrl+S PgUp</kbd> : This had me stumped for a while. Mouse wheel scrolls thru the history - which is not what one wants most of the times.

## Terminal Emulators

I keep moving between these two.

### WezTerm

This is quite stable, and is my ~~fallback option while I explore Kitty.~~ daily driver.


### [[term.kitty]]
