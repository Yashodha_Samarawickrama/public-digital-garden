## See the output from `print` in pytest

If the test cases have `print()` statements, you see those, only in case of failure.
But sometime, you want to see they unconditionally (i.e. even when the tests pass)

The way to do this is use `-s` option. This option disables "per test capturing".
Read the [docs](https://docs.pytest.org/en/latest/capture.html#setting-capturing-methods-or-disabling-capturing)

## Resources

* Load testing using [locust.io](https://locust.io/)
