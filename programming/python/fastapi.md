## Honeybadger integration with FastAPI:

```python
import os
from fastapi import FastAPI
from starlette.requests import Request
import uvicorn

if os.environ.get('HONEYBADGER_API_KEY'):
    from honeybadger import honeybadger, Honeybadger
    api_key = os.environ.get('HONEYBADGER_API_KEY')
    # `force_report_data` is required if in dev setup
    # See HB documentation for details
    honeybadger.configure(api_key=api_key, force_report_data=True)

app = FastAPI()


@app.middleware("http")
async def honeybadger_middleware(request: Request, call_next):
    try:
        response = await call_next(request)
    except Exception as e:
        if os.environ.get('HONEYBADGER_API_KEY'):
            honeybadger.notify(e, context={'path': request.url.path})
        raise e

    return response
```

## OAuth2 with MS Azure (May work with github)

[This comment](https://github.com/tiangolo/fastapi/issues/12#issuecomment-776249974) explains 
in detail, with code snippets

## Useful Third Party packages

* [Users Management](https://fastapi-users.github.io/fastapi-users/) : Ready-to-use and customizable user management for FastAPI
  * Has OAuth support out of the box
* [CRUD Router](https://fastapi-crudrouter.awtkns.com): A dynamic FastAPI router that automatically creates routes CRUD for your models
