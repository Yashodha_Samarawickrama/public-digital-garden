## Handling packages

`dotnet` command can be used manage NuGet packages

Switch to the directory containing the project and then

* `dotnet add package <PACKAGE_NAME>`
  * Optionally, pass `-v <VERSION>` to install specific version of the package
* `dotnet list package`
* `dotnet remove package <PACKAGE_NAME>`
 
See the 
[documentation](https://learn.microsoft.com/en-us/nuget/consume-packages/install-use-packages-dotnet-cli) for details.
