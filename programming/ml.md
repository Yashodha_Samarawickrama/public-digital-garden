# Notes from Coursera course

## introduction

* Supervised Learning: we have a data set and already know what our correct output should look like
  * Regression: Predict value on a curve
  * Classification: Predict Discrete value output, usually 1 or 0
* Unsupervised Learning: Expected output unknown. 
  * No feedback based on the prediction results
  * Derive relation/structure by grouping the data into clusters
  * e.g.
    * Market segmentation
    * Social network analysis.
  * Non Clustering : Find relation within chaotic environment
    * Cocktail Party problem
      * Separate out two voices, one closer to mic other away from mic.

## Model representation

* $X^{(i)}$ represents input data $Y^{(i)}$ represents output value
  * These are not exponentiation
* A pair $(x^{(i)}, y^{(i)})$ is called a training example, and the dataset — a list of $m$ training examples $(x^{(i)},y^{(i)});i=1,...,m$ — is called a training set.
* We need to find function `h` so that `h(x)` returns good predicted `y`
* `h` is called `hypothesis` 

![Model Representation](assets/images/model_representation.png)

## Cost Function

* Squared Error Function Or Mean Squared Error

$J(θ0​,θ1​)=2m1​i=1∑m​(y^​i​−yi​)2=2m1​i=1∑m​(hθ​(xi​)−yi​)2$
