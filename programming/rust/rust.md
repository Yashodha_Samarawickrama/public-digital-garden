## Local Documentation

* `rustup doc` brings up **all** the local documentation including 
  * The Rust book
  * Rust by Example
  * Cargo book
* Inside your project, `cargo doc --open` opens your project's documentation
  * Without `--open` will just generate the documentation, which can be potentially shared with others as required.

## Data Types

* Arrays : Fixed size
  * When defining say days of the week, months of the year etc.
  * Initialize as :
    * `let a = [3; 5];` is same as `let a = [3, 3, 3, 3, 3];`
* Vector : Like array but can shrink and/or grow
  * Use `new()` or `vec!` to create
  * ```rust
   let mut numbers: Vec<i32> = Vec::new();
   let mut magic_numbers = vec![7i32, 42, 47, 45, 54];
  ```
* If unsure, use Vector

## Control Flow

* rust does not have `truthy` values. e.g. if `x` is `1`, then `if x` does not compile. `x` **must** be boolean

* `loop` can have optional label. See the [documentation](https://rust-book.cs.brown.edu/ch03-05-control-flow.html#loop-labels-to-disambiguate-between-multiple-loops)
```rust
'outer: loop {
.   ...
  loop {  // inner, unlabeled loop
    ...
    if some_condition {
      break; // Will break from inner/unlabeled loop
    }
    ...
    ...
    if some_other_condition {
      break 'outer'; // will break out of the outer loop
    }
    ...
  }
}
```

## Statements Vs Expressions

Statements do not return a value, expressions do.

> C and Ruby, where the assignment returns the value of the assignment. 
> In those languages, you can write x = y = 6 and have both x and y have 
> the value 6; that is not the case in Rust.

In a function, last expression is also a return value.

> If you add a semicolon to the end of an expression, you turn it into
> a statement, and it will then not return a value.

## Ownership

* [Cloning avoids Moves](https://rust-book.cs.brown.edu/ch04-01-what-is-ownership.html#cloning-avoids-moves)
* Useful when we need to reference the "original" data after it the ownership is moved
* `.clone` create a copy (which is used to move the oownership,leaving the original intact)



## Slice

> We create slices using a range within brackets by specifying
> `[starting_index..ending_index]`, where `starting_index` is the first position
> in the slice and `ending_index` is **one more than the last position** in the slice. 
