* [LetsEncrypt Certbot SSL For Ubuntu](https://dwij.net/complete-guide-letsencrypt-certbot-ssl-for-ubuntu/)
  * I've not tested it myself
  * Not sure which version of Ubuntu this works on. The Article is from mid-2020, so 20.04 LTS can be assumed.

# Enable `hibernate` 

Details in this [SO thread](https://askubuntu.com/a/1312421)

TL:DR : 

- Use `swapfile` for Ubuntu 17 onwards
  - `sudo dd if=/dev/zero of=/swapfile bs=1G count=8`
  - OR `sudo dd if=/dev/zero of=/swapfile bs=1M count=8192`
- Add entry for the `swapfile` to `/etc/fstab`
- Update `/etc/default/grub`
  - `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash resume=UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX resume_offset=XXXXX"`
  - Use offset from `sudo filefrag -v /swapfile |grep " 0:"| awk '{print $4}'`. I had to remove `..` from the output
  - Use UUID of `/` from `cat /etc/fstab`
  - `sudo update-grub`
- Finally `sudo systemctl hibernate` : This did **not** work for me 😞 Keeping here for reference, hopefully someone finds it useful 🤞

# Access Virtual Console

I had problems when my entire Desktop would become slow/unresponsive.
Normally I would kill some process to make the DE responsive, but I can't even reach the terminal app.

In such cases, <kbd>Ctrl-Alt-F1</kbd> gives me access to virtual console.
Usually there are multiple consoles available from `F1` to `F6`

`F7` would bring you back to the DE.


[[Ubuntu.Budgie]]
[[Ubuntu.DDE]]