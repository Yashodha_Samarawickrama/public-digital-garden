# How to add entry to the Application Launcher

The only way to install Firefox Nightly is to download the tar.gz file. This means I always need to start the application from the terminal.

It would be nice to be able to start it from the application launcher like other applications. 

Menu looks for launchers first in `/usr/share/applications` then in `~/.local/share/applications`
My preference is to create entry under `~/.local/share/applications` - but for Desktop linux machine, where you are single owner, one might as well add it to `/usr/share/applications` (You might need `sudo` access)

TL;DR: Create a `.desktop` file under `.local/share/applications`

[Reference](https://discourse.ubuntubudgie.org/t/how-can-i-add-an-application-in-budgie-menu/2207/2)

# Install a Simple Calculator

I was surprised that Budgie did not come with a Calculator app. Later I realized that I had chosen *minimal* option during the installation.
So I installed `deepin-calculator`.

# PlayOnLinux

Since I had *minimal* install, I didn't have LibreOffice. TBH, I've used it in past and didn't really like it.
During the search for an alternative, I came across PlayOnLinux.

I successfully installed old (2015) version of Acrobat Reader DC. But it was just to see what the fuss is all about. I have a decent PDF reader in the form of *Atril Document Viewer*

# macOS keyboard layout on Linux

Kinto works quite nicely. I did face little bit of issue because I was using `fish` (So default install instruction did not work at first. So I opened `bash` session and off I went) Second issue was due to `pyenv` - I needed to use system installed version of python. The issue was discussed on their github repo [here](https://github.com/rbreaves/kinto/issues/388)

# Setup Bluetooth Audio

For some reason, just enabling Bluetooth via Settings did not work. It would go back to Disabled state. Following worked for me :

```sh
sudo vi /etc/bluetooth/audio.conf
```

Add the following

```
[General]
Enable=Source,Sink,Media,Socket
```

Save the file and start the Bluetooth service again.

```sh
sudo service bluetooth restart
```

[Resource](https://dev.to/campbelljones74/common-bluetooth-problems-in-ubuntu-and-how-to-fix-them-18b5)
