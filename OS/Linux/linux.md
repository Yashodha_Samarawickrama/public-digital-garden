# Backup

* [Borg Backup](https://borgbackup.readthedocs.io/en/stable/index.html) 
  * Supports "data deduplicated backup" (Or what we used to call differential backup). So daily backups only backup what has changed.
  * Back be backed up to local filesystem or remote
  * Supports encryption


[[Manjaro]]
[[Ubuntu]]
[[Regolith]]