UbuntuDDE is a special distro that ships DDE as default DE

## Run in docked mode

I needed to keep working using external monitor (keyboard and mouse) after I close the laptop.

`sudo vi /etc/systemd/logind.conf`

Uncomment the following:

```
HandleLidSwitch=ignore
HandleLidSwitchDocked=ignore
LidSwitchIgnoreInhibited=yes
```

Then `systemctl restart systemd-logind.service`

Now when I close the laptop, the external screen goes blank for few seconds, and then I see the lock screen. When I enter the password to unlock, I can continue to work using the external monitor (keyboard and mouse)

---

I also had to set the `scaling` to 1.0. The default was 1.25 which made everything so big that it was unusable.
